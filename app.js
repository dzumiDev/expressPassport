var mongoose = require('mongoose');
var express = require('express');
var expressSession = require('express-session');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var keys = require('./config/keys');
var morgan      = require('morgan');
var app = express();
// passport
var passport = require('passport');
// models
require('./models/user');

// connect mongodb
mongoose.connect(keys.mongoUri, function (err) {
  if (err) throw err;
  console.log('connect mongodb establish');
});
app.use(bodyParser.json());
// app.use(
//   cookieSession({
//     maxAge: 30 * 24 * 60 * 60 * 1000,
//     keys: [keys.cookieKey]
//   })
// );
// auth
require('./auth/passport');
app.use(expressSession({
  secret: keys.secret
}));
app.use(passport.initialize());
app.use(passport.session());
// Add headers fix local server
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});
// use morgan to log requests to the console
app.use(morgan('dev'));

// router
require('./routes/auth')(app);
app.listen(keys.port);