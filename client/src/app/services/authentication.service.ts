import { Injectable } from '@angular/core';
import { HelperService } from '../shared/helper.service';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(
    private helper: HelperService,
    private cookieService: CookieService,
    public jwtHelper: JwtHelperService
  ) { }

  /**
   * API authentication
   * @param info info login form
   * @returns {Observable<any>} Reference observable.
   */
  authentication(info): Observable<any> {
    return this.helper.makeRequest('post', '/auth/login', info, {});
  }

  /**
   * API sign up
   * @param info info login form
   * @returns {Observable<any>} Reference observable.
   */
  signUp(info): Observable<any> {
    return this.helper.makeRequest('post', '/auth/signup', info, {});
  }

  // validate token authentication
  isAuthenticated(): boolean {
    const token = this.cookieService.get('token');
    console.log(this.jwtHelper.isTokenExpired(token));
    console.log(this.jwtHelper.getTokenExpirationDate(token));
    return !this.jwtHelper.isTokenExpired(token);
  }
}
