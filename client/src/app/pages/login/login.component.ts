import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private typeInput = 'password';

  private txtUserName: string;
  private errorUserName = false;

  private txtPassword: string;
  private errorPassword = false;

  @ViewChild('inputUserName') inputUserName: any;
  @ViewChild('inputPassword') inputPassword: any;
  constructor(
    private router: Router,
    private _el: ElementRef,
    private authSvc: AuthenticationService,
    private cookieService: CookieService
  ) {

  }
  ngOnInit() {
    this.txtUserName = '';
    this.txtPassword = '';
  }

  loginFunc(): void {
    if (this.inputUserName.hasError('required')) {
      this.errorUserName = true;
    }

    if (this.inputPassword.hasError('required')) {
      this.errorPassword = true;
    }

    if (!this.errorUserName && !this.errorPassword) {
      const info = {
        username: this.txtUserName,
        password: this.txtPassword
      };
      this.authSvc.authentication(info).subscribe(
        res => {
          this.cookieService.set('token', res.token);
          this.router.navigate(['/dashboard']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  showPwd(): void {

  }
}
