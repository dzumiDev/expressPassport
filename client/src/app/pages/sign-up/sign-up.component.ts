import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// import { SliderService } from '../../services/slider.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  @Output() callbackFunc = new EventEmitter();
  private showText = false;
  private typeInput = 'password';
  private dataSlider: any[] = [];

  private txtUserName: string;
  private errorUserName = false;

  private txtEmail: string;
  private errorEmail = false;

  private txtPassword: string;
  private errorPassword = false;

  private txtConfirmPassword: string;
  private errorConfirmPassword = false;

  @ViewChild('inputUserName') inputUserName: any;
  @ViewChild('inputEmail') inputEmail: any;
  @ViewChild('inputPassword') inputPassword: any;
  @ViewChild('inputConfirmPassword') inputConfirmPassword: any;
  constructor(
    public router: Router,
    // private sliderSvc: SliderService,
  ) { }

  ngOnInit() {
    // this.sliderSvc.getDataSliderLogin().then(result => {
    //   this.dataSlider = result;
    // });
    this.txtUserName = '';
    this.txtPassword = '';
  }

  signUpFunc(): void {
    if (this.inputUserName.hasError('required')) {
      this.errorUserName = true;
    }

    if (this.inputEmail.hasError('required') || this.inputEmail.hasError('pattern')) {
      this.errorEmail = true;
    }

    if (this.inputPassword.hasError('required')) {
      this.errorPassword = true;
    }

    if (this.inputConfirmPassword.hasError('required')) {
      this.errorConfirmPassword = true;
    }

    if (this.txtPassword !== this.txtConfirmPassword) {
      this.errorPassword = true;
      this.errorConfirmPassword = true;
    }

    if (!this.errorUserName && !this.errorPassword && !this.errorEmail && !this.errorConfirmPassword) {
      localStorage.setItem('userName', this.txtUserName);
      this.callbackFunc.emit('close');
      this.router.navigate(['/profileSetup']);
    }
  }
  showTypeInput(event: any): void {
    if (event.currentTarget.text === 'show') {
      event.currentTarget.parentElement.querySelector('input').type = 'text';
      event.currentTarget.text = 'hide';
    } else {
      event.currentTarget.parentElement.querySelector('input').type = 'password';
      event.currentTarget.text = 'show';
    }
  }

  closeSignUpFunc() {
    this.callbackFunc.emit('close');
  }
}
