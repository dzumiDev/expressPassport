import { Directive, AfterViewInit, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appSetheighcenter]'
})
export class SetheighcenterDirective implements AfterViewInit {
  constructor(private el: ElementRef) {
  }
  ngAfterViewInit(): void {
    this.setHeightFunc();
  }
  // listen window resize
  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    this.setHeightFunc();
  }
  // set height function
  setHeightFunc(): void {
    const that = this;
    this.el.nativeElement.style.height = 'auto';
    setTimeout(function () {
      const heightElement = that.el.nativeElement.offsetHeight;
      const heightWindow = window.innerHeight;
      that.el.nativeElement.style.top = (heightWindow - heightElement)/2 + 'px';
    }, 50);
  }

}
