import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class HelperService {
  url = 'http://localhost:3000';
  headers = new HttpHeaders();
  constructor(
    private http: HttpClient
  ) {}

  /**
    * make request api backend
    * @param {string} method url to api
    * @param {string} url url to api
    * @param {any} body data
    * @param {any} optionValue option
    * @returns {Observable<any>} observable.
  */
  makeRequest(method, url, body, optionValue): Observable<any> {
    let option;
    if (!optionValue) {
      this.headers.append('Content-Type', 'application/json');
      option = {
        'headers': this.headers
      };
    } else {
      option = optionValue;
    }

    if (method === 'get') {
      return this.http.get(this.url + url, option);
    } else if (method === 'post') {
      console.log(this.url + url);
      console.log(body);
      return this.http.post(this.url + url, body, option);
    } else if (method === 'put') {
      return this.http.put(this.url + url, body, option);
    } else if (method === 'delete') {
      return this.http.delete(this.url + url, option);
    }
  }
}
