var controller = require('../controller/user.controller');
var passport = require('passport');
var authSvc = require('../auth/auth.service');
module.exports = app => {
  app.post('/auth/login', (req, res, next) => {
    passport.authenticate('local', (err, check, user) => {
      if (err) {
        return res.status(401).json(error);
      }
      if (!check) {
        return res.status(404).json({
          message: 'Something went wrong, please try again.'
        });
      }
      const token = authSvc.signToken(user._id, user.role);
      res.status(200).json({
        'role': user.role,
        'token': token
      });
    })(req, res, next);
  });

  /* create user*/
  app.post('/auth/signup', (req, res, next) => {
    passport.authenticate('signup', (err, check, user) => {
      if (err) {
        return res.status(401).json(err);
      }
      if (!check) {
        return res.status(404).json(info);
      }
      res.status(200).send(info);
    })(req, res, next);
  });

  // get all users
  app.get('/api/users', controller.users);
}