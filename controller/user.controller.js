var User = require('../models/user');
var controller = {};

// get all user
controller.users = (req, res) => {
  // get all the users
  User.find({}, function (err, users) {
    if (err) throw err;
    // object of all the users
    res.send(users);
  });
}

// // create user
// controller.create = (req, res) => {
//   var newUser = new User();
//   newUser.username = req.body.username;
//   newUser.password = req.body.password;
//   newUser.role = req.body.role;

//   // save the user
//   newUser.save(function (err) {
//     if (err) throw err;
//     res.send(newUser);
//   });
// }

module.exports = controller;