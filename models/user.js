var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

// create a schema
var userSchema = new Schema({
  username: String,
  password: String,
  role: String
});

// Tạo mã hóa mật khẩu
userSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// kiểm tra mật khẩu có trùng khớp
userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};


var User = mongoose.model('User', userSchema);
module.exports = User;