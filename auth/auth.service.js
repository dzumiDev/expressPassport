var jwt = require('jsonwebtoken');
var key = require('../config/keys');

var authSvc = {};

authSvc.signToken = (id, role) => {
  return jwt.sign({_id: id, role }, key.session, {
    algorithm: 'HS256',
    expiresIn: key.session_duration
  })
}

// authSvc.hasRole

module.exports = authSvc;