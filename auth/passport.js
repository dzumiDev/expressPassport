var passport = require('passport');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var LocalStrategy = require('passport-local').Strategy;
// var User = mongoose.model('users');
var User = require('../models/user');

// serialize User id
passport.serializeUser((user, done) => {
  done(null, user.id);
});
// deserialize User by id
passport.deserializeUser((id, done) => {
  User.findById(id).then(user => {
    done(null, user)
  }).catch(function (err) {
    console.log(err);
  });
})
// login
passport.use('local', new LocalStrategy(
  function (username, password, done) {
    User.findOne({
      username: username
    }).then(function (user) {
      bcrypt.compare(password, user.password, function (err, result) {
        if (err) {
          return done(err)
        }
        if (!result) {
          return done(null, false, {
            message: 'Incorrect username and password'
          })
        }
        return done(null, true, user)
      })
    }).catch(function (err) {
      return done(null, false, {
        message: 'Incorrect username and password'
      });
    })
  }
));

// sign up
passport.use('signup', new LocalStrategy({
    passReqToCallback: true
  },
  function (req, username, password, done) {
    findOrCreateUser = function () {
      // find a user in Mongo with provided username
      User.findOne({
        'username': username
      }, function (err, user) {
        // In case of any error return
        if (err) {
          return done(err);
        }
        // already exists
        if (user) {
          return done(null, false, {
            message: 'User already exists'
          });
        } else {
          // if there is no user with that email
          // create the user
          var newUser = new User();
          // set the user's local credentials
          newUser.username = req.body.username;
          newUser.password = newUser.generateHash(req.body.password);
          newUser.role = req.body.role;
          // save the user
          newUser.save(function (err) {
            if (err) {
              throw err;
            }
            return done(null, true, newUser);
          });
        }
      });
    };

    // Delay the execution of findOrCreateUser and execute 
    // the method in the next tick of the event loop
    process.nextTick(findOrCreateUser);
  }
));